package com.ninja_squad.example.project.test.junit5;

import com.ninja_squad.dbsetup.extension.annotations.*;
import com.ninja_squad.dbsetup.extension.utils.ResourceUtils;
import com.ninja_squad.dbsetup.extension.junit5.DbDataInjectExtension;
import com.ninja_squad.dbsetup.extension.junit5.DbInitExtension;
import com.ninja_squad.dbsetup.operation.Insert;
import com.ninja_squad.example.project.main.dao.ProjectTable;
import com.ninja_squad.example.project.main.dao.UsersDao;
import com.ninja_squad.example.project.main.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

@FileDataSource(location = "classpath:application-test.properties", prefix = "db.default")
@CreateSchema(random = true, prefix = "TEST")
@DDLScript(location="classpath:ddl.sql")
public class ProjectUsersDaoExample5Test {

    @RegisterExtension
    public static DbInitExtension dbInit = DbInitExtension.builder().build();

    @RegisterExtension
    public static DbDataInjectExtension data = dbInit.injectDataRule()
            .truncateTables(ProjectTable.values())
            .withOperation(
                    Insert.into("users")
                    .columns("id", "username", "firstname", "lastname")
                    .values("1", "root", "admin", "admin")
                    .values("2", "msoares", "Mario", "Soares")
                    .values("3", "jcottet", "Julien", "Cottet")
                    .build()
            ).build();

    private final UsersDao usersDao;

    public ProjectUsersDaoExample5Test(@SchemaName String schemaName) {
        final Properties properties = ResourceUtils.loadProperties("classpath:application-test.properties");
        DriverManagerDataSource ds = new DriverManagerDataSource(
                properties.getProperty("db.default.url"),
                properties.getProperty("db.default.username"),
                properties.getProperty("db.default.password"));
        ds.setSchema(schemaName);
        this.usersDao = new UsersDao(ds);
    }

    @ReadOnly
    @Test
    public void should_return_all_users() {
        final List<User> allUsers = usersDao.getAllUsers();
        assertEquals(3, allUsers.size());
    }

    @ReadOnly
    @Test
    public void should_return_one_user_identified_by_id() {
        final User user = usersDao.findById(2l);
        assertEquals("msoares", user.getUsername());
        assertEquals("Mario", user.getFirstname());
        assertEquals("Soares", user.getLastname());
    }

    @Test
    public void should_add_user() {
        User thomas = new User("tbasnier", "Thomas", "Basnier");
        final User user = usersDao.addUser(thomas);
        assertEquals(4l, user.getId().longValue());
        assertEquals(4, usersDao.getAllUsers().size());
    }

    @Test
    public void should_delete_user() {
        final int delete = usersDao.delete(3l);
        assertEquals(1, delete);
        assertEquals(2, usersDao.getAllUsers().size());
    }

    @Test
    public void should_update_user() {
        final User admin = usersDao.findById(1l);
        admin.setUsername("blotaut");
        admin.setFirstname("Brian");
        admin.setLastname("Lotaut");
        final int update = usersDao.updateUser(admin);
        assertEquals(1, update);
        assertEquals(3, usersDao.getAllUsers().size());
        final User brian = usersDao.findById(1l);
        assertEquals("blotaut", brian.getUsername());
        assertEquals("Brian", brian.getFirstname());
        assertEquals("Lotaut", brian.getLastname());
    }


}