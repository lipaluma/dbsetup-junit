package com.ninja_squad.example.project.main.dao;

import com.ninja_squad.dbsetup.extension.DbTable;

public enum ProjectTable implements DbTable {
    USERS("users"), ROLES("roles"), USERS_ROLES("users_roles");

    private String name;
    ProjectTable(String name) {
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }
}
