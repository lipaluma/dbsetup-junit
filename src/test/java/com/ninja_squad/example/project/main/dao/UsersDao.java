package com.ninja_squad.example.project.main.dao;

import com.ninja_squad.example.project.main.dao.mapper.UserMapper;
import com.ninja_squad.example.project.main.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

public class UsersDao {

    private JdbcTemplate template;

    @Autowired
    public UsersDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }


    public List<User> getAllUsers() {
        return template.query("select * from users", new UserMapper());
    }

    public User findById(Long id) {
        return template.queryForObject("select * from users where id = ?", new UserMapper(), id);
    }

    public int delete(Long id) {
        return template.update("delete from users where id = ?", id);
    }

    public User addUser(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement ps = connection.prepareStatement("insert into users (username, firstname, lastname) values (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getLastname());
            return ps;
        }, keyHolder);
        user.setId(keyHolder.getKey().longValue());
        return user;
    }

    public int updateUser(User user) {
        return template.update("update users set username=?, firstname=?, lastname=? where id = ?",
                user.getUsername(), user.getFirstname(), user.getLastname(), user.getId());
    }
}
