create table users(id int(11) primary key auto_increment, username varchar(100), firstname varchar(100), lastname varchar(100));

create table roles(id int(11) primary key auto_increment, libelle varchar(100));
-- insert into roles(id, libelle) values (1, 'ADMIN');
-- insert into roles(id, libelle) values (2, 'DEVELOPPER');
-- insert into roles(id, libelle) values (3, 'COMMERCIAL');
-- insert into roles(id, libelle) values (4, 'DIRECTOR');

create table users_roles(user_id int(11), role_id int(11));
-- insert into users_roles (user_id, role_id) values (1, 1);

