package com.ninja_squad.dbsetup.extension.retriever.sql_scripts;

import com.ninja_squad.dbsetup.extension.annotations.DDLScript;
import com.ninja_squad.dbsetup.extension.annotations.DataScript;
import com.ninja_squad.dbsetup.extension.exceptions.SqlScriptsInitializerException;
import com.ninja_squad.dbsetup.operation.Operation;
import com.ninja_squad.dbsetup.operation.SqlOperation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public interface SqlScriptsRetriever {

    List<File> getFiles();
    String getEncoding();
    String scanUntil();

    static SqlScriptsRetriever createFromAnnotations(Class<?> clazz) {
        if(clazz.isAnnotationPresent(DDLScript.class))
            return new AnnotationSqlScriptsRetriever(clazz.getAnnotation(DDLScript.class));
        if(clazz.isAnnotationPresent(DataScript.class))
            return new AnnotationSqlScriptsRetriever(clazz.getAnnotation(DataScript.class));
        return null;
    }

    default List<Operation> getOperations() {
        ArrayList<Operation> operations = new ArrayList<>();
        for(File file: getFiles()) {
            try {
                String sql = new Scanner(file, getEncoding()).useDelimiter(scanUntil()).next();
                operations.add(SqlOperation.of(sql));
            } catch (FileNotFoundException e) {
                throw new SqlScriptsInitializerException("Unable to load script sql file : "+file, e);
            }
        }
        return operations;
    }

}
