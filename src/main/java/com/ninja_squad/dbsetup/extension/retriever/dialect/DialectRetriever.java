package com.ninja_squad.dbsetup.extension.retriever.dialect;

import com.ninja_squad.dbsetup.extension.annotations.CustomDialect;
import com.ninja_squad.dbsetup.extension.dialect.Dialect;

public interface DialectRetriever {
    Dialect getDialect();

    static DialectRetriever createFromAnnotations(Class<?> clazz) {
        if(clazz.getAnnotation(com.ninja_squad.dbsetup.extension.annotations.Dialect.class) != null)
            return new AnnotationDialectRetriever(clazz.getAnnotation(com.ninja_squad.dbsetup.extension.annotations.Dialect.class));
        if(clazz.getAnnotation(CustomDialect.class) != null)
            return new CustomDialectRetriever(clazz.getAnnotation(CustomDialect.class));
        return null;
    }

}
