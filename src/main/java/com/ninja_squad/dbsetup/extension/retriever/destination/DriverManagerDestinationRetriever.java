package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;
import com.ninja_squad.dbsetup.extension.annotations.DataSource;

public class DriverManagerDestinationRetriever implements DestinationRetriever {

    private String url;
    private String username;
    private String password;

    public DriverManagerDestinationRetriever(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    public DriverManagerDestinationRetriever(DataSource annotation) {
        this.url = annotation.url();
        this.username = annotation.username();
        this.password = annotation.password();
    }

    @Override
    public Destination getDestination() {
        return new DriverManagerDestination(url, username, password);
    }

}
