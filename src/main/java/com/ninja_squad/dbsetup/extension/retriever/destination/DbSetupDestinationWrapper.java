package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.Destination;

import java.sql.Connection;
import java.sql.SQLException;

public class DbSetupDestinationWrapper implements Destination {
    private Destination destination;
    private String schema;

    public DbSetupDestinationWrapper(Destination destination) {
        this.destination = destination;
    }
    public DbSetupDestinationWrapper(Destination destination, String schema) {
        this.destination = destination;
        this.schema = schema;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = destination.getConnection();
        if(schema != null)
            connection.setSchema(schema);
        return connection;
    }
}
