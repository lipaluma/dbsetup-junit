package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.extension.annotations.FileDataSource;
import com.ninja_squad.dbsetup.extension.utils.ResourceUtils;

import java.io.File;

public class FileDataSourceAnnotationDestinationRetriever implements DestinationRetriever {
    private FileDataSource annotation;

    public FileDataSourceAnnotationDestinationRetriever(FileDataSource annotation) {
        this.annotation = annotation;
    }

    @Override
    public Destination getDestination() {
        File file = ResourceUtils.getResourceOrFile(annotation.location());
        DestinationRetriever retriever = new PropertiesFileDestinationRetriever(ResourceUtils.loadProperties(file), annotation.prefix());
        return retriever.getDestination();
    }

}
