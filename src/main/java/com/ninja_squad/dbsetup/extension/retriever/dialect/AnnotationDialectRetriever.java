package com.ninja_squad.dbsetup.extension.retriever.dialect;

import com.ninja_squad.dbsetup.extension.dialect.Dialect;

public class AnnotationDialectRetriever implements DialectRetriever {

    private com.ninja_squad.dbsetup.extension.annotations.Dialect annotation;

    public AnnotationDialectRetriever(com.ninja_squad.dbsetup.extension.annotations.Dialect annotation) {
        this.annotation = annotation;
    }

    @Override
    public Dialect getDialect() {
        return this.annotation.value();
    }
}
