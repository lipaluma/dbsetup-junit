package com.ninja_squad.dbsetup.extension.retriever.sql_scripts;

import com.ninja_squad.dbsetup.extension.annotations.DDLScript;
import com.ninja_squad.dbsetup.extension.annotations.DataScript;
import com.ninja_squad.dbsetup.extension.utils.ResourceUtils;

import java.io.File;
import java.util.List;

public class AnnotationSqlScriptsRetriever implements SqlScriptsRetriever {
    private DefaultSqlScriptsRetriever fileRetriever;

    public AnnotationSqlScriptsRetriever(DDLScript annotation) {
        File file = ResourceUtils.getResourceOrFile(annotation.location());
        this.fileRetriever = new DefaultSqlScriptsRetriever(file);
        this.fileRetriever.setEncoding(annotation.encoding());
        this.fileRetriever.scanUntil(annotation.scanUntil());
    }
    public AnnotationSqlScriptsRetriever(DataScript annotation) {
        File file = ResourceUtils.getResourceOrFile(annotation.location());
        this.fileRetriever = new DefaultSqlScriptsRetriever(file);
        this.fileRetriever.setEncoding(annotation.encoding());
        this.fileRetriever.scanUntil(annotation.scanUntil());
    }

    @Override
    public List<File> getFiles() {
        return this.fileRetriever.getFiles();
    }

    @Override
    public String getEncoding() {
        return fileRetriever.getEncoding();
    }

    @Override
    public String scanUntil() {
        return fileRetriever.scanUntil();
    }

}
