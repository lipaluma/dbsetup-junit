package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.extension.annotations.DataSource;
import com.ninja_squad.dbsetup.extension.annotations.FileDataSource;


/**
 * Class that gives the destination (datasource ou drivermanager) to use during the tests
 */
public interface DestinationRetriever {

    /**
     * get destination to use
     * @see com.ninja_squad.dbsetup.destination.DataSourceDestination
     * @see com.ninja_squad.dbsetup.destination.DriverManagerDestination
     * @return
     */
    Destination getDestination();

    static DestinationRetriever createFromAnnotations(Class<?> clazz) {
        if(clazz.getAnnotation(DataSource.class) != null)
            return new DriverManagerDestinationRetriever(clazz.getAnnotation(DataSource.class));
        if(clazz.getAnnotation(FileDataSource.class) != null)
            return new FileDataSourceAnnotationDestinationRetriever(clazz.getAnnotation(FileDataSource.class));
        return null;
    }
}
