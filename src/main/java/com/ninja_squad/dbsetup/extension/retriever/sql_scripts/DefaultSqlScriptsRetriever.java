package com.ninja_squad.dbsetup.extension.retriever.sql_scripts;

import com.ninja_squad.dbsetup.extension.Constants;
import com.ninja_squad.dbsetup.extension.exceptions.SqlScriptsInitializerException;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultSqlScriptsRetriever implements SqlScriptsRetriever {
    private final List<File> files;
    private String encoding = Constants.DEFAULT_ENCODING;
    private String pattern = Constants.DEFAULT_PATTERN;

    public DefaultSqlScriptsRetriever(File file) {
        if(!file.exists())
            throw new SqlScriptsInitializerException("MPD File or folder "+file.getAbsolutePath()+" not found.");
        if(file.isDirectory()) {
            this.files = Arrays.stream(file.listFiles((dir, name) -> name.endsWith(".sql"))).sorted(Comparator.comparing(File::getName)).collect(Collectors.toList());
        } else
            this.files = Arrays.asList(file);
    }

    @Override
    public List<File> getFiles() {
        return this.files;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    public void scanUntil(String pattern){
        this.pattern = pattern;
    }

    @Override
    public String scanUntil() {
        return pattern;
    }
}
