package com.ninja_squad.dbsetup.extension.retriever.dialect;

import com.ninja_squad.dbsetup.extension.dialect.Dialect;

public class DefinedDialectRetriever implements DialectRetriever {

    private Dialect dialect;

    public DefinedDialectRetriever(Dialect dialect) {
        this.dialect = dialect;
    }

    @Override
    public Dialect getDialect() {
        return this.dialect;
    }
}
