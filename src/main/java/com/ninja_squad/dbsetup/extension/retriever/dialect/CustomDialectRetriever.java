package com.ninja_squad.dbsetup.extension.retriever.dialect;

import com.ninja_squad.dbsetup.extension.annotations.CustomDialect;
import com.ninja_squad.dbsetup.extension.dialect.Dialect;
import com.ninja_squad.dbsetup.extension.exceptions.DialectInitialisationException;

public class CustomDialectRetriever implements DialectRetriever {

    private Dialect dialect;

    public CustomDialectRetriever(CustomDialect annotation) {
        final Class<? extends Dialect> clazz = annotation.value();
        try {
            this.dialect = clazz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new DialectInitialisationException("Error on initialisation of dialect from class "+clazz, e);
        }

    }

    @Override
    public Dialect getDialect() {
        return this.dialect;
    }
}
