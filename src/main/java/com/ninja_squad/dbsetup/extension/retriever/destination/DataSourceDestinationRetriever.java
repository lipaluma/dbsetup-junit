package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.destination.Destination;

import javax.sql.DataSource;

public class DataSourceDestinationRetriever implements DestinationRetriever {

    private DataSource dataSource;

    public DataSourceDestinationRetriever(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Destination getDestination() {
        return new DataSourceDestination(dataSource);
    }

}
