package com.ninja_squad.dbsetup.extension.retriever.dialect;

import com.ninja_squad.dbsetup.extension.dialect.Dialect;
import com.ninja_squad.dbsetup.extension.dialect.DialectEnum;

public class DefaultDialectRetriever implements DialectRetriever {
    @Override
    public Dialect getDialect() {
        return DialectEnum.DEFAULT;
    }
}
