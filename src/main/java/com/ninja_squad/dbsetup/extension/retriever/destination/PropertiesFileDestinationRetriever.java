package com.ninja_squad.dbsetup.extension.retriever.destination;

import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;

import java.util.Properties;
import java.util.function.Function;

public class PropertiesFileDestinationRetriever implements DestinationRetriever {

    private Properties properties;
    private String prefix;
    private Function<String, String> decryptFunction;

    public PropertiesFileDestinationRetriever(Properties properties, String prefix) {
        this.properties = properties;
        this.prefix = prefix;
    }

    public void decryptPasswordWith(Function<String, String> decryptFunction) {
        this.decryptFunction = decryptFunction;
    }

    @Override
    public Destination getDestination() {
        String url = properties.getProperty(prefix+".url");
        String username = properties.getProperty(prefix+".username");
        String password = properties.getProperty(prefix+".password");
        if(decryptFunction != null)
            password = decryptFunction.apply(password);
        return new DriverManagerDestination(url, username, password);
    }

}
