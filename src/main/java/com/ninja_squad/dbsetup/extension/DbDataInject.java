package com.ninja_squad.dbsetup.extension;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.bind.BinderConfiguration;
import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.extension.annotations.DataScript;
import com.ninja_squad.dbsetup.extension.annotations.ReadOnly;
import com.ninja_squad.dbsetup.extension.builder.InitDataSourceBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitDataSourceStepByStepBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitScriptSqlBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitScriptsSqlStepByStepBuilder;
import com.ninja_squad.dbsetup.extension.exceptions.DbDestinationInitialisationException;
import com.ninja_squad.dbsetup.extension.retriever.destination.DestinationRetriever;
import com.ninja_squad.dbsetup.extension.retriever.sql_scripts.AnnotationSqlScriptsRetriever;
import com.ninja_squad.dbsetup.extension.retriever.sql_scripts.SqlScriptsRetriever;
import com.ninja_squad.dbsetup.operation.CompositeOperation;
import com.ninja_squad.dbsetup.operation.Operation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class DbDataInject<PARENT extends DbInit> extends DbSetupRule {

    protected PARENT parent;
    protected DestinationRetriever destinationRetriever;
    protected DbSetupTracker setupTracker;
    protected DbSetup dataSetup;

    protected SqlScriptsRetriever sqlScriptsRetriever;
    protected CleanStrategy cleanStrategy;
    protected List<String> tableNames;
    protected Operation dataOperation;
    protected BinderConfiguration binderConfiguration;

    public DbDataInject() {}
    protected <T> DbDataInject(DbDataInject.Builder<T, PARENT> builder) {
        this.parent = builder.parent;
        this.destinationRetriever = builder.destinationRetriever;
        this.sqlScriptsRetriever = builder.sqlScriptsRetriever;
        this.cleanStrategy = builder.strategy;
        this.tableNames = builder.tables;
        this.dataOperation = builder.operation;
        this.binderConfiguration = builder.binderConfiguration;
    }

    protected void init(DbInit dbInit, Class<?> testClass) {
        if(this.destinationRetriever == null)
            this.destinationRetriever = dbInit.getDestinationRetriever();
        if(this.binderConfiguration == null)
            this.binderConfiguration = dbInit.binderConfiguration;
        this.setupTracker = dbInit.setupTracker;
        DataScript annotation = testClass.getAnnotation(DataScript.class);
        Destination destination = this.destinationRetriever.getDestination();
        List<Operation> cleanOperations = getCleanOperations(destination, annotation, dbInit.getSchemaName());
        List<Operation> dataOperations = initAllDataOperations(annotation);
        this.dataSetup = setupOf(destination,
            CompositeOperation.sequenceOf(
                dbInit.getDialect().changeSchema(dbInit.getSchemaName()),
                CompositeOperation.sequenceOf(cleanOperations),
                CompositeOperation.sequenceOf(dataOperations)
            ),
            this.binderConfiguration
        );
    }

    protected void init(Class<?> testClass) {
        if(this.destinationRetriever == null)
            throw new DbDestinationInitialisationException("No datasource defined on "+this.getClass().getName());
        this.setupTracker = new DbSetupTracker();
        DataScript annotation = testClass.getAnnotation(DataScript.class);
        Destination destination = this.destinationRetriever.getDestination();
        List<Operation> cleanOperation = getCleanOperations(destination, annotation, null);
        List<Operation> dataOperations = initAllDataOperations(annotation);
        this.dataSetup = setupOf(destination,
            CompositeOperation.sequenceOf(
                CompositeOperation.sequenceOf(cleanOperation),
                CompositeOperation.sequenceOf(dataOperations)
            ),
            this.binderConfiguration
        );
    }

    private List<Operation> getCleanOperations(Destination destination, DataScript annotation, String schema) {
        if(this.cleanStrategy == null && annotation != null)
            this.cleanStrategy = annotation.cleanStrategy();
        if(this.tableNames == null && annotation != null)
            this.tableNames = Arrays.asList(annotation.cleanTables());
        if(this.tableNames == null || this.tableNames.isEmpty())
            this.tableNames = new ArrayList<>();
        try (Connection connection = destination.getConnection()){
            ResultSet rs = connection.getMetaData().getTables(null, schema, "%", new String[] {"TABLE"});
            while(rs.next())
                this.tableNames.add(rs.getString(3));
        } catch (Exception e) {
            return new ArrayList<>();
        }
        return Arrays.asList(this.cleanStrategy.cleanAllTables(this.tableNames));
    }

    private List<Operation> initAllDataOperations(DataScript annotation) {
        List<Operation> dataOperations = initDataOperationsByAnnotation(annotation);
        if(this.sqlScriptsRetriever != null)
            dataOperations.addAll(this.sqlScriptsRetriever.getOperations());
        if(this.dataOperation != null)
            dataOperations.add(dataOperation);
        return dataOperations;
    }

    private List<Operation> initDataOperationsByAnnotation(DataScript annotation) {
        if(annotation == null)
            return new ArrayList<>();
        AnnotationSqlScriptsRetriever dataFilesRetriever = new AnnotationSqlScriptsRetriever(annotation);
        return dataFilesRetriever.getOperations();
    }

    protected void launchSetupIfNeeded() {
        setupTracker.launchIfNecessary(dataSetup);
    }
    protected void skipNextLaunchIfNeeded(Class<?> testClass) {
        if (testClass.getAnnotation(ReadOnly.class) != null)
            setupTracker.skipNextLaunch();
    }

    public static abstract class Builder<T, PARENT> implements InitScriptSqlBuilder<T>, InitDataSourceBuilder<T> {

        private PARENT parent;
        private Operation operation;
        private List<String> tables;
        private CleanStrategy strategy;
        private BinderConfiguration binderConfiguration;
        private DestinationRetriever destinationRetriever;
        private SqlScriptsRetriever sqlScriptsRetriever;

        public Builder() { }
        public Builder(PARENT parent) {
            this.parent = parent;
        }

        public InitDataSourceStepByStepBuilder<T, Builder<T, PARENT>> declareDestination() {
            return new InitDataSourceStepByStepBuilder<>(this);
        }
        @Override
        public Builder<T, PARENT> withDestinationRetriever(DestinationRetriever destinationRetriever) {
            this.destinationRetriever = destinationRetriever;
            return this;
        }

        public Builder<T, PARENT> withOperation(Operation operation) {
            this.operation = operation;
            return this;
        }
        public Builder<T, PARENT> deleteTables(String... tables) {
            this.tables = Arrays.asList(tables);
            this.strategy = CleanStrategy.DELETE;
            return this;
        }
        public Builder<T, PARENT> deleteTables(DbTable... tables) {
            this.tables = Arrays.stream(tables).map(DbTable::getName).collect(Collectors.toList());
            this.strategy = CleanStrategy.DELETE;
            return this;
        }
        public Builder<T, PARENT> deleteTables(List<String> tables) {
            this.tables = tables;
            this.strategy = CleanStrategy.DELETE;
            return this;
        }

        public Builder<T, PARENT> truncateTables(String... tables) {
            this.tables = Arrays.asList(tables);
            this.strategy = CleanStrategy.TRUNCATE;
            return this;
        }
        public Builder<T, PARENT> truncateTables(DbTable... tables) {
            this.tables = Arrays.stream(tables).map(DbTable::getName).collect(Collectors.toList());
            this.strategy = CleanStrategy.TRUNCATE;
            return this;
        }
        public Builder<T, PARENT> truncateTables(List<String> tables) {
            this.tables = tables;
            this.strategy = CleanStrategy.TRUNCATE;
            return this;
        }

        @Override
        public Builder<T, PARENT> withSqlScriptsRetriever(SqlScriptsRetriever sqlScriptsRetriever) {
            this.sqlScriptsRetriever = sqlScriptsRetriever;
            return this;
        }
        public InitScriptsSqlStepByStepBuilder<T, Builder<T, PARENT>> declareSqlScripts() {
            return new InitScriptsSqlStepByStepBuilder<>(this);
        }

        public Builder<T, PARENT> withBinderConfiguration(BinderConfiguration configuration) {
            this.binderConfiguration = configuration;
            return this;
        }

        public abstract T build();
    }

}
