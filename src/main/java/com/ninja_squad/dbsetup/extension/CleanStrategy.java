package com.ninja_squad.dbsetup.extension;

import com.ninja_squad.dbsetup.operation.DeleteAll;
import com.ninja_squad.dbsetup.operation.Operation;
import com.ninja_squad.dbsetup.operation.Truncate;

import java.util.List;

public enum CleanStrategy {
    DELETE{
        @Override
        public Operation cleanAllTables(List<String> tables) {
            return DeleteAll.from(tables);
        }
    },
    TRUNCATE {
        @Override
        public Operation cleanAllTables(List<String> tables) {
            return Truncate.tables(tables);
        }
    };

    public abstract Operation cleanAllTables(List<String> tables);

}
