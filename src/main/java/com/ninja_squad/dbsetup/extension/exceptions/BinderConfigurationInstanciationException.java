package com.ninja_squad.dbsetup.extension.exceptions;

public class BinderConfigurationInstanciationException extends RuntimeException {
    public BinderConfigurationInstanciationException(String message, Throwable cause) {
        super(message, cause);
    }
}
