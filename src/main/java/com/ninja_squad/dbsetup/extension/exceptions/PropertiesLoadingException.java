package com.ninja_squad.dbsetup.extension.exceptions;

public class PropertiesLoadingException extends RuntimeException {
    public PropertiesLoadingException(String message) {
        super(message);
    }

    public PropertiesLoadingException(String message, Throwable cause) {
        super(message, cause);
    }
}
