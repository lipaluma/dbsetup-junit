package com.ninja_squad.dbsetup.extension.exceptions;

public class SqlScriptsInitializerException extends RuntimeException {
    public SqlScriptsInitializerException(String message) {
        super(message);
    }

    public SqlScriptsInitializerException(String message, Throwable cause) {
        super(message, cause);
    }
}
