package com.ninja_squad.dbsetup.extension.exceptions;

public class DbInitializerRuleException extends RuntimeException {
    public DbInitializerRuleException(String message) {
        super(message);
    }

    public DbInitializerRuleException(String message, Throwable cause) {
        super(message, cause);
    }
}
