package com.ninja_squad.dbsetup.extension.exceptions;

public class DbDestinationInitialisationException extends RuntimeException {
    public DbDestinationInitialisationException(String message) {
        super(message);
    }

    public DbDestinationInitialisationException(String message, Throwable cause) {
        super(message, cause);
    }
}
