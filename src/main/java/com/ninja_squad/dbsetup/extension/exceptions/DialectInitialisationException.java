package com.ninja_squad.dbsetup.extension.exceptions;

public class DialectInitialisationException extends RuntimeException {
    public DialectInitialisationException(String message) {
        super(message);
    }

    public DialectInitialisationException(String message, Throwable cause) {
        super(message, cause);
    }
}
