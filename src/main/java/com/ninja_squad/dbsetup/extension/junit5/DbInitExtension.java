package com.ninja_squad.dbsetup.extension.junit5;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.extension.DbInit;
import com.ninja_squad.dbsetup.extension.annotations.SchemaName;
import org.junit.jupiter.api.extension.*;

public class DbInitExtension extends DbInit implements BeforeAllCallback, AfterAllCallback, ParameterResolver {

    public DbInitExtension(){
        super();
    }
    private DbInitExtension(Builder builder) {
        super(builder);
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        init(extensionContext.getRequiredTestClass());
        extensionContext.getStore(ExtensionContext.Namespace.create(extensionContext.getRequiredTestClass())).put("DbInitExtension", this);
        initializeSetup().launch();
        if(initSchemaConsumer != null)
            initSchemaConsumer.accept(schemaName);
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) {
        finalizeSetup().ifPresent(DbSetup::launch);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType().equals(String.class) && parameterContext.isAnnotated(SchemaName.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return this.schemaName;
    }

    public DbDataInjectExtension.Builder injectDataRule() {
        return DbDataInjectExtension.builder(this);
    }

    public static DbInitExtension.Builder builder() {
        return new DbInitExtension.Builder();
    }

    public static class Builder extends DbInit.Builder<DbInitExtension> {
        public DbInitExtension build() {
            return new DbInitExtension(this);
        }
    }

}
