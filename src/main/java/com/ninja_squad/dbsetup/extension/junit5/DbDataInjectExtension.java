package com.ninja_squad.dbsetup.extension.junit5;

import com.ninja_squad.dbsetup.extension.DbDataInject;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class DbDataInjectExtension extends DbDataInject<DbInitExtension> implements BeforeAllCallback, BeforeEachCallback, AfterEachCallback {
    public DbDataInjectExtension() {
        super();
    }
    public DbDataInjectExtension(DbDataInject.Builder<DbDataInjectExtension, DbInitExtension> builder) {
        super(builder);
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        DbInitExtension extension = context.getStore(ExtensionContext.Namespace.create(context.getRequiredTestClass()))
            .get("DbInitExtension", DbInitExtension.class);
        if(extension != null) {
            init(extension, context.getRequiredTestClass());
        } else {
            init(context.getRequiredTestClass());
        }
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        launchSetupIfNeeded();
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) {
        skipNextLaunchIfNeeded(extensionContext.getRequiredTestClass());
    }

    public static DbDataInjectExtension.Builder builder() {
        return new DbDataInjectExtension.Builder();
    }
    static DbDataInjectExtension.Builder builder(DbInitExtension rule) {
        return new DbDataInjectExtension.Builder(rule);
    }

    public static class Builder extends DbDataInject.Builder<DbDataInjectExtension, DbInitExtension> {
        public Builder() {
            super();
        }
        public Builder(DbInitExtension rule) {
            super(rule);
        }

        public DbDataInjectExtension build(){
            return new DbDataInjectExtension(this);
        }
    }
}
