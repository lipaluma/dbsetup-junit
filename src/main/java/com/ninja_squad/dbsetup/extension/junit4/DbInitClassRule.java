package com.ninja_squad.dbsetup.extension.junit4;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.extension.DbInit;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Class Rule used to init database by creating new schema and tables used for the test.
 * To initialize a {@link DbInitClassRule}, use the {@link Builder}
 *
 */
public class DbInitClassRule extends DbInit implements TestRule {


    private DbInitClassRule(Builder builder) {
        super(builder);
    }

    public DbDataInjectRule.Builder injectDataRule() {
        return DbDataInjectRule.builder(this);
    }

    /**
     * Method invoked automatically by junit when the class test is launched before his instanciation. It is run in static context.<br/>
     *
     * Here, we finalize initialization of the class rule, create the schema with tables before launching all tests.<br/>
     *
     * At the end of all tests, the schema created is dropped except if 'keepSchema' was setted to true.
     *
     * @param base
     * @param description
     * @return
     */
    @Override
    public Statement apply(Statement base, Description description) {
        init(description.getTestClass());
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
            try {
                initializeSetup().launch();
                if(initSchemaConsumer != null)
                    initSchemaConsumer.accept(schemaName);
                base.evaluate();
            } finally {
                finalizeSetup().ifPresent(DbSetup::launch);
            }
            }
        };
    }

    /**
     * Class builder that create {@link DbInitClassRule}
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Class Builder that creates a new {@link DbInitClassRule}
     */
    public static class Builder extends DbInit.Builder<DbInitClassRule> {
        @Override
        public DbInitClassRule build() {
            return new DbInitClassRule(this);
        }


    }

}



