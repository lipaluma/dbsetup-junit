package com.ninja_squad.dbsetup.extension.junit4;

import com.ninja_squad.dbsetup.extension.DbDataInject;
import com.ninja_squad.dbsetup.extension.annotations.ReadOnly;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class DbDataInjectRule extends DbDataInject<DbInitClassRule> implements TestRule {

    private DbDataInjectRule(Builder builder) {
        super(builder);
    }

    /**
     * Method applied automatically by junit before each unit test.<br/>
     * Before each test, the data is refreshed in the database. This step can be skipped if the previous test is annotated with {@link ReadOnly}
     * @param base
     * @param description
     * @return
     */
    @Override
    public Statement apply(Statement base, Description description) {
        init(parent, description.getTestClass());
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    launchSetupIfNeeded();
                    base.evaluate();
                } finally {
                    skipNextLaunchIfNeeded();
                }
            }

            private void skipNextLaunchIfNeeded() {
                if (description.getAnnotation(ReadOnly.class) != null)
                    setupTracker.skipNextLaunch();
            }

            private void launchSetupIfNeeded() {
                setupTracker.launchIfNecessary(dataSetup);
            }
        };
    }


    /**
     * create a new Builder for {@link DbDataInjectRule}
     * @return
     */
    public static Builder builder() {
        return new Builder();
    }
    /**
     * create a new Builder for {@link DbDataInjectRule} already initialized with configuration given in the {@link DbInitClassRule}
     * @return
     */
    public static Builder builder(DbInitClassRule rule) {
        return new Builder(rule);
    }

    /**
     * Class builder that create {@link DbDataInjectRule}
     */
    public static class Builder extends DbDataInject.Builder<DbDataInjectRule, DbInitClassRule> {
        public Builder() { }
        public Builder(DbInitClassRule parent) {
            super(parent);
        }
        @Override
        public DbDataInjectRule build(){
            return new DbDataInjectRule(this);
        }
    }
}
