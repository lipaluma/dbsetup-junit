package com.ninja_squad.dbsetup.extension.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CreateSchema {
    boolean random() default false;

    String value() default "";

    String name() default "";

    String prefix() default "";

    boolean keepAtEnd() default false;
}
