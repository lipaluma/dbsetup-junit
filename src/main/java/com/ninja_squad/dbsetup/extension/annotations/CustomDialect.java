package com.ninja_squad.dbsetup.extension.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomDialect {
    Class <? extends com.ninja_squad.dbsetup.extension.dialect.Dialect> value();
}
