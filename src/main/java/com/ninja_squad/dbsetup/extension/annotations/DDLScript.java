package com.ninja_squad.dbsetup.extension.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DDLScript {
    String location();
    String encoding() default "UTF-8";
    String scanUntil() default "\\A";
}
