package com.ninja_squad.dbsetup.extension.annotations;

import com.ninja_squad.dbsetup.extension.CleanStrategy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScript {
    String location();
    String encoding() default "UTF-8";
    String scanUntil() default "\\A";

    String[] cleanTables() default {};
    CleanStrategy cleanStrategy() default CleanStrategy.TRUNCATE;
}
