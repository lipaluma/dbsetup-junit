package com.ninja_squad.dbsetup.extension.annotations;

import com.ninja_squad.dbsetup.bind.BinderConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Binder {
    Class<? extends BinderConfiguration> value();
}
