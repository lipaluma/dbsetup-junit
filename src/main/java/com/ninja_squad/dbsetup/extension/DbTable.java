package com.ninja_squad.dbsetup.extension;

/**
 * Interface that gives the name of a table in the database
 */
public interface DbTable {
    String getName();
}
