package com.ninja_squad.dbsetup.extension;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.bind.BinderConfiguration;
import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.extension.annotations.Binder;
import com.ninja_squad.dbsetup.extension.annotations.CreateSchema;
import com.ninja_squad.dbsetup.extension.builder.InitDataSourceBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitDataSourceStepByStepBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitScriptSqlBuilder;
import com.ninja_squad.dbsetup.extension.builder.InitScriptsSqlStepByStepBuilder;
import com.ninja_squad.dbsetup.extension.dialect.Dialect;
import com.ninja_squad.dbsetup.extension.dialect.DialectEnum;
import com.ninja_squad.dbsetup.extension.exceptions.BinderConfigurationInstanciationException;
import com.ninja_squad.dbsetup.extension.exceptions.DbDestinationInitialisationException;
import com.ninja_squad.dbsetup.extension.retriever.destination.DestinationRetriever;
import com.ninja_squad.dbsetup.extension.retriever.dialect.DefaultDialectRetriever;
import com.ninja_squad.dbsetup.extension.retriever.dialect.DefinedDialectRetriever;
import com.ninja_squad.dbsetup.extension.retriever.dialect.DialectRetriever;
import com.ninja_squad.dbsetup.extension.retriever.sql_scripts.SqlScriptsRetriever;
import com.ninja_squad.dbsetup.operation.CompositeOperation;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.platform.commons.util.StringUtils;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public abstract class DbInit extends DbSetupRule {
    protected DbSetupTracker setupTracker;
    protected BinderConfiguration binderConfiguration;
    protected SqlScriptsRetriever sqlScriptsRetriever;
    protected Consumer<String> initSchemaConsumer;
    protected DestinationRetriever destinationRetriever;
    protected DialectRetriever dialectRetriever;
    protected String schemaName;
    protected Boolean randomSchema;
    protected Boolean keepSchema;

    public DbInit(){
        this.setupTracker = new DbSetupTracker();
    }
    protected <T>DbInit(DbInit.Builder<T> builder) {
        this.setupTracker = new DbSetupTracker();
        this.binderConfiguration = builder.binderConfiguration;
        this.destinationRetriever = builder.destinationRetriever;
        this.sqlScriptsRetriever = builder.sqlScriptsRetriever;
        this.initSchemaConsumer = builder.initSchemaConsumer;
        this.dialectRetriever = builder.dialectRetriever;
        this.keepSchema = builder.keepSchema;
        this.schemaName = builder.schemaName;
        this.randomSchema = builder.randomSchema;
    }

    protected void init(Class<?> testClass) {
        checkAnnotationSchemaCreationInfo(testClass);
        checkDestination(testClass);
        checkDialect(testClass);
        checkDDL(testClass);
        checkBinderConfiguration(testClass);
        if(randomSchema) {
            if(schemaName != null)
                schemaName += "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss_SSS"));
            else
                schemaName = testClass.getSimpleName() + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss_SSS"));
        }
    }

    protected void checkAnnotationSchemaCreationInfo(Class<?> testClass) {
        if(testClass.isAnnotationPresent(CreateSchema.class)) {
            CreateSchema annotation = testClass.getAnnotation(CreateSchema.class);
            if(this.schemaName == null && StringUtils.isNotBlank(annotation.name()))
                this.schemaName = annotation.name();
            else if(this.schemaName == null && StringUtils.isNotBlank(annotation.value()))
                this.schemaName = annotation.value();
            if(this.randomSchema == null)
                this.randomSchema = annotation.random();
            if(this.keepSchema == null)
                this.keepSchema = annotation.keepAtEnd();
            if(this.randomSchema && StringUtils.isNotBlank(annotation.prefix())) {
                this.schemaName = annotation.prefix();
            }
        } else {
            if(this.randomSchema == null)
                this.randomSchema = false;
            if(this.keepSchema == null)
                this.keepSchema = false;
        }
    }

    protected void checkBinderConfiguration(Class<?> testClass) {
        if(this.binderConfiguration != null)
            return;
        final Binder annotation = testClass.getAnnotation(Binder.class);
        if(annotation != null) {
            try {
                this.binderConfiguration = annotation.value().getDeclaredConstructor().newInstance();
            } catch (Exception e) {
                throw new BinderConfigurationInstanciationException("Binder class can't be instanciated.", e);
            }
        }
    }

    protected void checkDestination(Class<?> testClass) {
        if(this.destinationRetriever == null) {
            this.destinationRetriever = DestinationRetriever.createFromAnnotations(testClass);
        }
        if(this.destinationRetriever == null) {
            throw new DbDestinationInitialisationException("No Destination (DataSource or DriverManager) initialized");
        }
    }

    protected void checkDialect(Class<?> testClass) {
        if(this.dialectRetriever != null)
            return;
        this.dialectRetriever = DialectRetriever.createFromAnnotations(testClass);
        if(this.dialectRetriever == null) {
            try(Connection connection = this.destinationRetriever.getDestination().getConnection()) {
                Driver driver = DriverManager.getDriver(connection.getMetaData().getURL());
                DialectEnum dialect = DialectEnum.getDialectFromDriver(driver);
                this.dialectRetriever = new DefinedDialectRetriever(dialect);
            } catch (SQLException e) {
                this.dialectRetriever = new DefaultDialectRetriever();
            }
        }
    }

    protected void checkDDL(Class<?> testClass) {
        if(this.sqlScriptsRetriever != null)
            return;
        this.sqlScriptsRetriever = SqlScriptsRetriever.createFromAnnotations(testClass);
    }

    protected DbSetup initializeSetup() {
        List<Operation> operations = new ArrayList<>();
        if(schemaName != null) {
            operations.add(this.dialectRetriever.getDialect().createSchema(this.schemaName));
            operations.add(this.dialectRetriever.getDialect().changeSchema(this.schemaName));
        }
        if(this.sqlScriptsRetriever != null)
            operations.addAll(this.sqlScriptsRetriever.getOperations());
        final Destination destination = this.destinationRetriever.getDestination();
        return setupOf(destination, CompositeOperation.sequenceOf(operations), this.binderConfiguration);
    }

    protected Optional<DbSetup> finalizeSetup() {
        if (!keepSchema && schemaName != null) {
            final Destination destination = this.destinationRetriever.getDestination();
            return Optional.of(setupOf(destination, this.dialectRetriever.getDialect().dropSchema(this.schemaName), this.binderConfiguration));
        } else
            return Optional.empty();
    }

    public String getSchemaName() {
        return schemaName;
    }
    public Dialect getDialect() {
        return dialectRetriever.getDialect();
    }
    public Destination getDestination() {
        return this.destinationRetriever.getDestination();
    }

    DestinationRetriever getDestinationRetriever() {
        return destinationRetriever;
    }


    public static abstract class Builder<T> implements InitDataSourceBuilder<T>, InitScriptSqlBuilder<T> {
        private String schemaName;
        private Boolean keepSchema;
        private Boolean randomSchema;
        private DestinationRetriever destinationRetriever;
        private SqlScriptsRetriever sqlScriptsRetriever;
        private DialectRetriever dialectRetriever;
        private BinderConfiguration binderConfiguration;
        private Consumer<String> initSchemaConsumer;


        public InitDataSourceStepByStepBuilder<T, Builder<T>> declareDestination() {
            return new InitDataSourceStepByStepBuilder<>(this);
        }

        public Builder<T> initSchema(Consumer<String> initSchemaConsumer) {
            this.initSchemaConsumer = initSchemaConsumer;
            return this;
        }

        public InitScriptsSqlStepByStepBuilder<T, Builder<T>> declareDDL() {
            return new InitScriptsSqlStepByStepBuilder<>(this);
        }

        @Override
        public Builder<T> withDestinationRetriever(DestinationRetriever destinationRetriever) {
            this.destinationRetriever = destinationRetriever;
            return this;
        }

        @Override
        public Builder<T> withSqlScriptsRetriever(SqlScriptsRetriever sqlScriptsRetriever) {
            this.sqlScriptsRetriever = sqlScriptsRetriever;
            return this;
        }

        public Builder<T> withDialect(Dialect dialect) {
            this.dialectRetriever = new DefinedDialectRetriever(dialect);
            return this;
        }

        public Builder<T> createRandomSchema() {
            this.randomSchema = true;
            return this;
        }
        public Builder<T> createRandomSchema(boolean keepAtEnd) {
            this.createRandomSchema();
            this.keepSchema = keepAtEnd;
            return this;
        }
        public Builder<T> createRandomSchema(String prefix) {
            this.schemaName = prefix;
            this.randomSchema = true;
            return this;
        }
        public Builder<T> createRandomSchema(String prefix, boolean keepAtEnd) {
            this.createRandomSchema(prefix);
            this.keepSchema = keepAtEnd;
            return this;
        }

        public Builder<T> createSchema(String name) {
            this.schemaName = name;
            return this;
        }
        public Builder<T> createSchema(String name, boolean keepAtEnd) {
            this.createSchema(name);
            this.keepSchema = keepAtEnd;
            return this;
        }

        public Builder<T> withBinderConfiguration(BinderConfiguration binderConfiguration) {
            this.binderConfiguration = binderConfiguration;
            return this;
        }


        public abstract T build();
    }

}
