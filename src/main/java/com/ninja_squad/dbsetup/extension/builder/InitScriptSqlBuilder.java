package com.ninja_squad.dbsetup.extension.builder;

import com.ninja_squad.dbsetup.extension.retriever.sql_scripts.SqlScriptsRetriever;

public interface InitScriptSqlBuilder<T> {
    InitScriptSqlBuilder<T> withSqlScriptsRetriever(SqlScriptsRetriever retriever);
}
