package com.ninja_squad.dbsetup.extension.builder;

import com.ninja_squad.dbsetup.extension.retriever.destination.DestinationRetriever;

public interface InitDataSourceBuilder<T> {
    InitDataSourceBuilder<T> withDestinationRetriever(DestinationRetriever destinationRetriever);
}
