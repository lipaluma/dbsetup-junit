package com.ninja_squad.dbsetup.extension.builder;

import com.ninja_squad.dbsetup.extension.retriever.destination.DataSourceDestinationRetriever;
import com.ninja_squad.dbsetup.extension.retriever.destination.DriverManagerDestinationRetriever;
import com.ninja_squad.dbsetup.extension.retriever.destination.PropertiesFileDestinationRetriever;
import com.ninja_squad.dbsetup.extension.utils.ResourceUtils;

import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;
import java.util.function.Function;

/**
 * builder focus on initialisation of destination (datasource, drivermanager)
 * @param <PARENT> the builder to continue initialisation
 */
public class InitDataSourceStepByStepBuilder<T, PARENT extends InitDataSourceBuilder<T>> {
        private PARENT parent;

        public InitDataSourceStepByStepBuilder(PARENT parent) {
            this.parent = parent;
        }

        /**
         * declare destination by datasource
         * @param dataSource datasource
         * @return builder
         */
        public PARENT withDataSource(DataSource dataSource) {
            final DataSourceDestinationRetriever destinationRetriever = new DataSourceDestinationRetriever(dataSource);
            parent.withDestinationRetriever(destinationRetriever);
            return parent;
        }

        /**
         * declare destination by declaring new {@link java.sql.DriverManager}
         * @param url url of database
         * @param username username to login in the database
         * @param password password associated to the login
         * @return builder
         */
        public PARENT withDriverManager(String url, String username, String password) {
            parent.withDestinationRetriever(new DriverManagerDestinationRetriever(url, username, password));
            return parent;
        }

        /**
         * declare destination using a properties file.
         * @link ByPropertiesDataSourceBuilder
         * @return builder
         */
        public ByPropertiesDataSourceBuilder byProperties() {
            return new ByPropertiesDataSourceBuilder(this);
        }

    /**
     * Class that declare destination using a properties file.<br/>
     * This file must contain these fields :
     * <ul>
     *     <li>{prefix}.url : mandatory. The url of the database</li>
     *     <li>{prefix}.username : optional. The username to login</li>
     *     <li>{prefix}.password : optional. The password associated to the username</li>
     *     <li>{prefix}.driver : optional. The driver class</li>
     * </ul>
     * <br/>
     * The prefix is parameterized. By default, the prefix is datasource. Then, by default, the properties search keys should like :
     * <ul>
     *     <li>datasource.url=...</li>
     *     <li>datasource.username=...</li>
     *     <li>datasource.password=...</li>
     *     <li>datasource.driver=...</li>
     * </ul>
     * @return builder
     */
    public class ByPropertiesDataSourceBuilder {
        private String prefix = "datasource";
        private InitDataSourceStepByStepBuilder<T, PARENT> subParent1;
        private Properties properties;
        private Function<String, String> decryptFunction;

        private ByPropertiesDataSourceBuilder(InitDataSourceStepByStepBuilder<T, PARENT> parent) {
            this.subParent1 = parent;
        }

        /**
         * define location of the properties file.
         * @param path
         * @return builder
         */
        public ByPropertiesDataSourceBuilder onLocation(String path) {
            return onLocation(ResourceUtils.getResourceOrFile(path));
        }

        /**
         * define location of the properties file.
         * @param file
         * @return builder
         */
        public ByPropertiesDataSourceBuilder onLocation(File file) {
            return withProperties(ResourceUtils.loadProperties(file));
        }

        /**
         * define properties directly without passing an external file.
         * @param properties
         * @return builder
         */
        public ByPropertiesDataSourceBuilder withProperties(Properties properties) {
            this.properties = properties;
            return this;
        }

        /**
         * modify the prefix of properties file
         * @param prefix
         * @return builder
         */
        public ByPropertiesDataSourceBuilder withPrefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        /**
         * add a decryption function to decrypt the given password
         * @param decryptFn
         * @return builder
         */
        public ByPropertiesDataSourceBuilder decryptPasswordWith(Function<String, String> decryptFn) {
            this.decryptFunction = decryptFn;
            return this;
        }

        /**
         * finalize declaration of the datasource by file
         * @return builder
         */
        public PARENT end() {
            final PropertiesFileDestinationRetriever retriever = new PropertiesFileDestinationRetriever(properties, prefix);
            retriever.decryptPasswordWith(decryptFunction);
            subParent1.parent.withDestinationRetriever(retriever);
            return subParent1.parent;
        }
    }
}

