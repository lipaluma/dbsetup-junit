package com.ninja_squad.dbsetup.extension.builder;

import com.ninja_squad.dbsetup.extension.exceptions.SqlScriptsInitializerException;
import com.ninja_squad.dbsetup.extension.retriever.sql_scripts.DefaultSqlScriptsRetriever;
import com.ninja_squad.dbsetup.extension.utils.ResourceUtils;

import java.io.File;

/**
 * builder used to define sql files of creation table to launch at the init of the test.
 * @param <PARENT>
 */
public class InitScriptsSqlStepByStepBuilder<T, PARENT extends InitScriptSqlBuilder<T>> {
    public static final String DEFAULT_ENCODING = "UTF-8";

    protected File file;
    private PARENT parent;
    private String encoding = DEFAULT_ENCODING;
    private String pattern = "\\A";


    public InitScriptsSqlStepByStepBuilder(PARENT parent) {
        this.parent = parent;
    }

    /**
     * define the encoding of the sql files
     * @param encoding
     * @return builder
     */
    public InitScriptsSqlStepByStepBuilder<T, PARENT> encodedIn(String encoding) {
        this.encoding = encoding;
        return this;
    }

    /**
     * define location of the sql files.<br/>
     * It can be one sql file or a folder that contains several sql files. In this case, the files are sorted by name to know the order how the files have to be launched.
     *
     * @param file the sql file or the path to the folder
     * @return builder
     */
    public InitScriptsSqlStepByStepBuilder<T, PARENT> onLocation(File file) {
        this.file = file;
        return this;
    }

    /**
     * define location of the sql files.<br/>
     * the word "classpath:" can be mentioned on starting of the path to indicate a file or folder in the classpath.
     * It can be one sql file or a folder that contains several sql files. In this case, the files are sorted by name to know the order how the files have to be launched.
     *
     * @param path the sql file or the path to the folder
     * @return builder
     */
    public InitScriptsSqlStepByStepBuilder<T, PARENT> onLocation(String path) {
        File resource = ResourceUtils.getResourceOrFile(path);
        if(resource == null)
            throw new SqlScriptsInitializerException("Folder of file "+path+" not found");
        return onLocation(resource);
    }

    /**
     * By default, all content in the files will be played.<br/>
     * But a pattern can be specified to don't play sql operations that appears after.
     * @param pattern
     * @return builder
     */
    public InitScriptsSqlStepByStepBuilder<T, PARENT> scanUntil(String pattern) {
        this.pattern = pattern;
        return this;
    }

    public PARENT end() {
        DefaultSqlScriptsRetriever retriever = new DefaultSqlScriptsRetriever(this.file);
        retriever.setEncoding(encoding);
        retriever.scanUntil(pattern);
        parent.withSqlScriptsRetriever(retriever);
        return parent;
    }

}
