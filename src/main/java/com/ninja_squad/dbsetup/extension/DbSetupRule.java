package com.ninja_squad.dbsetup.extension;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.bind.BinderConfiguration;
import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.operation.Operation;

public abstract class DbSetupRule {

    protected DbSetup setupOf(Destination destination, Operation operation) {
        return setupOf(destination, operation, null);
    }

    protected DbSetup setupOf(Destination destination, Operation operation, BinderConfiguration configuration) {
        if(configuration == null)
            return new DbSetup(destination, operation);
        else
            return new DbSetup(destination, operation, configuration);
    }

}
