package com.ninja_squad.dbsetup.extension.dialect;

import com.ninja_squad.dbsetup.operation.Operation;

import java.sql.Driver;

public enum DialectEnum implements Dialect {
    DEFAULT(new DefaultDialect()),
    MYSQL(new MySqlDialect()),
    POSTGRES(new PostGresDialect()),
    H2(new H2Dialect())
    //, DB2, ORACLE, SQLSERVER
    ;

    private final Dialect impl;

    DialectEnum(Dialect impl) {
        this.impl = impl;
    }

    public static DialectEnum getDialectFromDriver(Driver driver) {
        for(DialectEnum dialect: values()) {
            if(driver.getClass().getCanonicalName().toLowerCase().contains(dialect.name().toLowerCase()))
                return dialect;
        }
        return DEFAULT;
    }

    @Override
    public Operation createSchema(String schemaName) {
        return impl.createSchema(schemaName);
    }

    @Override
    public Operation changeSchema(String schemaName) {
        return impl.changeSchema(schemaName);
    }

    @Override
    public Operation dropSchema(String schemaName) {
        return impl.dropSchema(schemaName);
    }
}
