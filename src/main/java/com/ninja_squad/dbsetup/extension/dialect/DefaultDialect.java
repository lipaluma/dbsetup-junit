package com.ninja_squad.dbsetup.extension.dialect;

import com.ninja_squad.dbsetup.operation.Operation;
import com.ninja_squad.dbsetup.operation.SqlOperation;

public class DefaultDialect implements Dialect {

    @Override
    public Operation createSchema(String schemaName) {
        return SqlOperation.of("CREATE SCHEMA IF NOT EXISTS " + schemaName);
    }

    @Override
    public Operation changeSchema(String schemaName) {
        return SqlOperation.of("SET SCHEMA " + schemaName);
    }

    @Override
    public Operation dropSchema(String schemaName) {
        return SqlOperation.of("DROP SCHEMA " + schemaName);
    }

}
