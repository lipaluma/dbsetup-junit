package com.ninja_squad.dbsetup.extension.dialect;

import com.ninja_squad.dbsetup.operation.Operation;
import com.ninja_squad.dbsetup.operation.SqlOperation;

public class MySqlDialect implements Dialect {

    @Override
    public Operation createSchema(String schemaName) {
        return SqlOperation.of("CREATE DATABASE IF NOT EXISTS " + schemaName);
    }

    @Override
    public Operation changeSchema(String schemaName) {
        return SqlOperation.of("USE " + schemaName);
    }

    @Override
    public Operation dropSchema(String schemaName) {
        return SqlOperation.of("DROP DATABASE " + schemaName);
    }


}
