package com.ninja_squad.dbsetup.extension.dialect;

import com.ninja_squad.dbsetup.operation.Operation;

public interface Dialect {
    Operation createSchema(String schemaName);
    Operation changeSchema(String schemaName);
    Operation dropSchema(String schemaName);
}
