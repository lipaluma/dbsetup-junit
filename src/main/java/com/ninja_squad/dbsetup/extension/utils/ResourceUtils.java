package com.ninja_squad.dbsetup.extension.utils;

import com.ninja_squad.dbsetup.extension.exceptions.PropertiesLoadingException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * Util Class permits search files and load properties from file in the classpath or not.
 */
public class ResourceUtils {

    /**
     * get File from classpath.<br/>
     * Returns null if not found
     * @param path of the file
     * @return the file. null if not found
     */
    public static File getResource(String path) {
        final URL resource = ClassLoader.getSystemResource(path);
        return loadResource(resource);
    }

    /**
     * get File from the package of the class.<br/>
     * Returns null if not found
     * @param path of the file
     * @param clazz class to get the package from where to search
     * @return the file. null if not found
     */
    public static File getResource(String path, Class<?> clazz) {
        final URL resource = clazz.getResource(path);
        return loadResource(resource);
    }

    /**
     * get from resource from classpath if "classpath:" is mentioned in the path.<br>
     * If not, returns the file on the absolute path
     * @param path the path of file
     * @return the File.
     */
    public static File getResourceOrFile(String path) {
        if(path.startsWith("classpath:")) {
            return getResource(path.substring("classpath:".length()));
        } else {
            return new File(path);
        }
    }

    /**
     * load a propeties file given in the path. The path can mention "classpath:" to search the file in the classpath instead.
     * @param path the path of the properties file
     * @return Properties loaded
     */
    public static Properties loadProperties(String path) {
        File file = ResourceUtils.getResourceOrFile(path);
        return loadProperties(file);
    }

    /**
     * load a propeties file.
     * @return Properties loaded
     */
    public static Properties loadProperties(File file) {
        try {
            Properties props = new Properties();
            props.load(new FileReader(file));
            return props;
        } catch (IOException e) {
            throw new PropertiesLoadingException("Properties file '"+file+"' can't be load", e);
        }
    }

    private static File loadResource(URL resource) {
        if(resource != null) {
            try {
                String result = resource.toURI().getPath();
                return new File(result);
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException("Path "+resource.getPath()+" can't be loaded as resource", e);
            }
        }
        else
            return null;
    }

}
